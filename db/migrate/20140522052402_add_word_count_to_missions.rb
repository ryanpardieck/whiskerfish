class AddWordCountToMissions < ActiveRecord::Migration
  def change
    add_column :missions, :word_count, :integer
  end
end
