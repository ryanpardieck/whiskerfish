class AddBodyToMissions < ActiveRecord::Migration
  def change
    add_column :missions, :body, :text
  end
end
