$ ->

  $(".new_mission #mission_body").keyup ->

    countWords = (words) ->
      words = words.replace /(^\s*)|(\s*$)/gi, ""
      words = words.replace /[ ]{2,}/gi," "
      words = words.replace /\n /,"\n"
      words = words.split(' ').length

    text = $(".new_mission #mission_body").val()
    $("#words-remaining").text (200 - countWords(text)).toString()

