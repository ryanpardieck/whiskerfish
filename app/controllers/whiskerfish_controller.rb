class WhiskerfishController < ApplicationController
  def index
    @missions = Mission.all
    @total_word_count = Mission.total_word_count
  end
end