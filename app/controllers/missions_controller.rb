class MissionsController < ApplicationController 
  def new
    @mission = Mission.new
  end

  def create
    Mission.create!(mission_params)
    redirect_to root_path
  end

  private 

  def mission_params
    params[:mission].permit(:title, :body)
  end
  
end