class Mission < ActiveRecord::Base
  def self.total_word_count
    all_missions = self.all
    return 0 if all_missions == []
    word_counts = all_missions.map do |m|
      m.word_count
    end
    word_counts.reduce :+
  end

  def word_count
    #TODO:Is there a need for a better word count solution?
    return 0 if !self.body
    self.body.split.size
  end
end
