# -*- coding: utf-8 -*-
require 'spec_helper'

feature 'FEATURE: Create a new mission page:' do
  before do
    Mission.destroy_all
  end

  #scenario 'User can go back to home page'

end

feature 'FEATURE: Words-remaining counter on new mission page:', js: true do
  before do
    Mission.destroy_all
  end

  scenario 'Counter exists and starts at +200 when blank' do
    visit root_path
    click_link 'Add a mission'

    expect(page).to have_content('Words Remaining: 200')
  end

  scenario 'Counter goes to +190 when 10 words are entered' do 
    visit root_path
    click_link 'Add a mission'

    fill_in 'mission_body', with: 'I once owned a very, very silly dog named Dog.'
    page.execute_script %Q{ $('#mission_body').trigger('keyup') }

    expect(page).to have_content('Words Remaining: 190')
  end

  scenario 'Counter goes to +173 when 27 words are entered' do
    visit root_path
    click_link 'Add a mission'

    huge_mistake_text = 'Now the story of a wealthy family who lost everything, ' <<
      'and the one son who had no choice but to keep them all together. ' <<
      'It\'s Arrested Development.'

    fill_in 'mission_body', with: huge_mistake_text
    page.execute_script %Q{ $('#mission_body').trigger('keyup') }

    expect(page).to have_content('Words Remaining: 173')
  end

  scenario 'Counter goes to +103 when 97 words are entered' do
    visit root_path
    click_link 'Add a mission'

    big_juicy_text = 'Oh my God, what if you wake up some day, ' <<
      'and you’re 65, or 75, and you never got your memoir or novel written; ' <<
      'or you didn’t go swimming in warm pools and oceans all those years ' <<
      'because your thighs were jiggly and you had a nice big comfortable tummy; ' <<
      'or you were just so strung out on perfectionism and people-pleasing that ' <<
      'you forgot to have a big juicy creative life, of imagination and radical ' <<
      'silliness and staring off into space like when you were a kid? It’s going ' <<
      'to break your heart. Don’t let this happen.'

    fill_in 'mission_body', with: big_juicy_text
    page.execute_script %Q{ $('#mission_body').trigger('keyup') }

    expect(page).to have_content('Words Remaining: 103')
  end

  scenario 'Counter goes to -139 when 339 words are entered' do
    visit root_path
    click_link 'Add a mission'

    joi_text = %Q{But that's not the way I ... that's not the way a real player plays. With respect and due effort and care for every point. You want to be great, near-great, you give every ball everything. And then some. You concede nothing. Even against loxes. You play right up to your limit and then pass your limit and look back at your former limit and wave a hankie at it, embarking. You enter a trance. You feel the seams and edges of everything. The court becomes a ... an extremely unique place to be. It will do every¬thing for you. It will let nothing escape your body. Objects move as they're made to, at the lightest easiest touch. You slip into the clear current of back and forth, making delicate X's and L's across the harsh rough bright green asphalt surface, your sweat the same temperature as your skin, playing with such ease and total mindless effortless effort and and and entranced concen¬tration you don't even stop to consider whether to run down every ball. You're barely aware you're doing it. Your body's doing it for you and the court and Game's doing it for your body. You're barely involved. ... Talent is its own expectation, Jim: you either live up to it or it waves a hankie, receding forever. Use it or lose it, he'd say over the newspaper. I'm ... I'm just afraid of having a tombstone that says HERE LIES A PROMISING OLD MAN. It's ... po-tential may be worse than none, Jim. Than no talent to fritter in the first place, lying around guzzling because I haven't the balls to ... God I'm I'm so sorry. Jim. You don't deserve to see me like this. I'm so scared, Jim. I'm so scared of dying without ever being really seen. Can you understand? Are you enough of a big thin prematurely stooped young bespectacled man, even with your whole life still ahead of you, to understand? Can you see I was giving it all I had?}

    fill_in 'mission_body', with: joi_text
    page.execute_script %Q{ $('#mission_body').trigger('keyup') }

    expect(page).to have_content('Words Remaining: -139')
  end
end
