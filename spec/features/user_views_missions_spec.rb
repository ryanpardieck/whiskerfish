require 'spec_helper'


#HUMAN
#WHEN:VISIT FRONT PAGE
#DO:GO TO FRONT PAGE
#Q:DO YOU SEE YOUR MISSIONS DISPLAYED?

feature 'CURRENT:FEATURE: User can see missions titles in sidebar links' do
  scenario 'A new mission title shows up as sidebar link' do
    visit root_path
    click_link 'Add a mission' #TODO:factor this setup out into helper
    fill_in 'Title', with: 'Never Slap Wild Animals'
    fill_in 'Body', with: 'Because Sometimes They Bite'
    click_button 'Save mission'

    within('.sidebar-nav .nav-list') do
      expect(page).to have_content('Never Slap Wild Animals')
    end
  end

  pending 'Clicking a mission in the sidebar cycles it to the top'
end

feature 'FEATURE: User can manage/view their daily mission statement' do

  before do
    Mission.destroy_all
  end

  scenario 'Viewing the 20 missions on main page' do 
    visit root_path

    expect(page).to have_content 'Your daily mission statement'
  end

  scenario 'User can add a mission to the statement scroll' do 
    visit root_path
    click_link 'Add a mission'
    fill_in 'Title', with: 'Love The Family'
    fill_in 'Body', with: 'Always Love The Family'
    click_button 'Save mission'

    within('#whiskerfish') do
      expect(page).to have_content('Love The Family')
    end
  end

  scenario 'User sees count of total words in mission statement' do
    visit root_path
    expect(page).to have_content('Word Count: 0')

    click_link 'Add a mission'
    fill_in 'Body', with: 'Always Love The Family'
    click_button 'Save mission'

    expect(page).to have_content('Word Count: 4')
  end
end
