require 'spec_helper'

describe Mission, :type => :model do
  describe '.word_count' do
    it 'should return the number of words in the body of the mission' do
      test_mission = Mission.create!
      test_mission.body = "Always trust the family. Remember all birthdays."

      expect(test_mission.word_count).to eq 7
    end
  end

  describe 'self.total_word_count' do
    before do 
      Mission.destroy_all
    end

    it 'should return the total word count of all the missions' do
      #better way to handle having Mission cleared? Is it needed?
      Mission.create! body: "I've made a huge mistake."
      Mission.create! body: "Always money in the banana stand."
      Mission.create! body: "There are dozens of us!"

      expect(Mission.total_word_count).to eq 16

    end
  end
end
